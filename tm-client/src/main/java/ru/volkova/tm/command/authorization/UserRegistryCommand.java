package ru.volkova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractAuthCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "registry new user in system";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @Nullable final Session session = bootstrap.getSession();
        endpointLocator.getAdminUserEndpoint().createUserWithEmail(session, login, password, email);
    }

    @NotNull
    @Override
    public String name() {
        return "user-registry";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
