package ru.volkova.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.marker.IntegrationCategory;

public class UserEndpointTest {

    private final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void setPasswordTest() {
        final String password = "123";
        endpointLocator.getUserEndpoint().setPassword(session, password);
        final Session session1 = endpointLocator.getSessionEndpoint().openSession("admin", password);
        Assert.assertNotNull(session1);
        endpointLocator.getUserEndpoint().setPassword(session1, "pass");
        endpointLocator.getSessionEndpoint().closeSession(session1);
    }

}
