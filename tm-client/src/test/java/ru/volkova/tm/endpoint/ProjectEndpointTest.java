package ru.volkova.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.marker.IntegrationCategory;

public class ProjectEndpointTest {

    private final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void addProjectByUserTest() {
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, "project", "test");
        project = endpointLocator.getProjectEndpoint()
                .findProjectById(session, project.id);
        Assert.assertNotNull(project);
        Assert.assertEquals("project", project.name);
        Assert.assertEquals("test", project.description);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByIdTest() {
        final Status status = Status.COMPLETE;
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, "project", "test");
        project = endpointLocator.getProjectEndpoint()
                .changeProjectOneStatusById(session, project.id, status);
        Assert.assertEquals(status, project.status);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByNameTest() {
        final Status status = Status.COMPLETE;
        final String name = "test-project";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, "test");
        project = endpointLocator.getProjectEndpoint()
                .changeProjectStatusByName(session, project.name, status);
        Assert.assertEquals(status, project.status);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertNotNull(endpointLocator.getProjectEndpoint()
                .findProjectById(session, project.id));
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertNotNull(endpointLocator.getProjectEndpoint()
                .findProjectById(session, project.id));
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void finishProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertEquals(Status.COMPLETE, endpointLocator.getProjectEndpoint()
                .finishProjectById(session, project.id).status);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void finishProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertEquals(Status.COMPLETE, endpointLocator.getProjectEndpoint()
                .finishProjectByName(session, project.name).status);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void startProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertEquals(Status.IN_PROGRESS, endpointLocator.getProjectEndpoint()
                .startProjectById(session, project.id).status);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void startProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertEquals(Status.IN_PROGRESS, endpointLocator.getProjectEndpoint()
                .startProjectByName(session, project.name).status);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(IntegrationCategory.class)
    public void removeProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
        endpointLocator.getProjectEndpoint().findProjectById(session, project.id);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(IntegrationCategory.class)
    public void removeProjectByNameTest(){
        final String name = "test-project1";
        final String description = "test-project1";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        endpointLocator.getProjectEndpoint().removeProjectByName(session, project.name);
        endpointLocator.getProjectEndpoint().findProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateProjectByIdTest() {
        final String name = "test-project";
        final String upd_name = "test";
        final String description = "test-project";
        final String upd_description = "test";
        Project project = endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        project = endpointLocator.getProjectEndpoint()
                .updateProjectById(session, project.id, upd_name, upd_description);
        Assert.assertEquals(upd_name, project.name);
        Assert.assertEquals(upd_description, project.description);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

}
