package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final User user = new User();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Project project = new Project();
        Assert.assertNotNull(projectRepository.add(project));
    }

    @Test
    @Category(UnitCategory.class)
    public void addWithParametersTest() {
        final Optional<Project> project = projectRepository.add("1", "Project 1", "it is project");
        Assert.assertNotNull(project);
        Assert.assertEquals("1", project.get().getUserId());
        Assert.assertEquals("Project 1", project.get().getName());
        Assert.assertEquals("it is project", project.get().getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Project> projectList = new ArrayList<Project>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        project1.setUserId(user.getId());
        project2.setUserId(user.getId());
        projectList.add(project1);
        projectList.add(project2);
        projectRepository.addAll(projectList);
        Assert.assertTrue(projectRepository.findById(project1.getUserId(),project1.getId()).isPresent());
        Assert.assertTrue(projectRepository.findById(project2.getUserId(),project2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.findAll(project.getUserId()));
        projectRepository.remove(project);
        Assert.assertTrue(projectRepository.findAll(project.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.findAll(project.getUserId()));
        projectRepository.clear(user.getId());
        Assert.assertTrue(projectRepository.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertTrue(projectRepository.findById(project.getUserId(), project.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIndexTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertTrue(projectRepository.findOneByIndex(project.getUserId(), 0).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByNameTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("DEMO");
        projectRepository.add(project);
        Assert.assertTrue(projectRepository.findOneByName(project.getUserId(), project.getName()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.findById(project.getUserId(), project.getId()));
        projectRepository.removeById(project.getUserId(), project.getId());
        Assert.assertTrue(projectRepository.findAll(project.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertTrue(projectRepository.findOneByIndex(project.getUserId(), 0).isPresent());
        projectRepository.removeOneByIndex(project.getUserId(), 0);
        Assert.assertTrue(projectRepository.findAll(project.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("DEMO");
        projectRepository.add(project);
        Assert.assertTrue(projectRepository.findOneByName(project.getUserId(), project.getName()).isPresent());
        projectRepository.removeOneByName(project.getUserId(), project.getName());
        Assert.assertTrue(projectRepository.findAll(project.getUserId()).isEmpty());
    }

}
