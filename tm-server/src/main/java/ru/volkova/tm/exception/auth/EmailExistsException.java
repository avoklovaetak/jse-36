package ru.volkova.tm.exception.auth;

import ru.volkova.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
