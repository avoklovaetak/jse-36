package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.entity.Task;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Optional<Task> add(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        findById(userId, taskId).ifPresent(task -> task.setProjectId(projectId));
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return entities.stream()
                .filter(predicateByUserIdAndProjectId(userId, projectId))
                .collect(Collectors.toList());
    }

    @NotNull
    protected final Predicate<Task> predicateByUserIdAndProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return task -> projectId.equals(task.getProjectId())
                && userId.equals(task.getUserId());
    }

    @Override
    public void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        findAllByProjectId(userId, projectId).forEach(this::remove);
    }

    @Override
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        findById(userId, taskId).ifPresent(task -> task.setProjectId(null));
    }

}
